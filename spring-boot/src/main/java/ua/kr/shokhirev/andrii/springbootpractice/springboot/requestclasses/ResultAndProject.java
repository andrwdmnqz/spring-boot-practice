package ua.kr.shokhirev.andrii.springbootpractice.springboot.requestclasses;

public class ResultAndProject {

    private String projectName;
    private String result;

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
