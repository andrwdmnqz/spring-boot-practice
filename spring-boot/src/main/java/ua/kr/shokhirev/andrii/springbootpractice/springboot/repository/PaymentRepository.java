package ua.kr.shokhirev.andrii.springbootpractice.springboot.repository;

import org.springframework.stereotype.Repository;
import ua.kr.shokhirev.andrii.springbootpractice.springboot.container.PaymentContainer;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Repository
public class PaymentRepository {

    private Set<PaymentContainer> payments = new HashSet<>();

    public Set<PaymentContainer> getAll() {
        return Collections.unmodifiableSet(payments);
    }

    public void add(PaymentContainer paymentContainer) {
        payments.add(paymentContainer);
    }
}
