package ua.kr.shokhirev.andrii.springbootpractice.springboot.container;

public class SmallPayment {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "SmallPayment{" +
                "name='" + name + '\'' +
                '}';
    }
}
