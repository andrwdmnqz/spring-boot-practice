package ua.kr.shokhirev.andrii.springbootpractice.springboot.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.kr.shokhirev.andrii.springbootpractice.springboot.container.PaymentContainer;
import ua.kr.shokhirev.andrii.springbootpractice.springboot.requestclasses.ResultAndProject;
import ua.kr.shokhirev.andrii.springbootpractice.springboot.requestclasses.Project;
import ua.kr.shokhirev.andrii.springbootpractice.springboot.service.PaymentService;

import java.util.Set;

@RestController
@RequestMapping("/guest")
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    @GetMapping("/count")
    public int getCount() {
        return paymentService.getCount();
    }

    @GetMapping("/projects")
    public ResponseEntity<? super PaymentContainer> projectsGetAllNames() {

        Set<Project> paymentContainerSet;

        try {
            paymentContainerSet = paymentService.getAllNamesList();
            if (paymentContainerSet.isEmpty()) {
                throw new RuntimeException();
            }
        } catch (RuntimeException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.ok(paymentContainerSet);
    }

    @GetMapping("/failed-results")
    public ResponseEntity<? super PaymentContainer> getFailedResults() {

        Set<ResultAndProject> failedResultSet;

        try {
            failedResultSet = paymentService.unsuccessfulResults();
            if (failedResultSet.isEmpty()) {
                throw new RuntimeException();
            }
        } catch (RuntimeException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.ok(failedResultSet);
    }

    @GetMapping("/success-results")
    public ResponseEntity<? super PaymentContainer> getSuccessResults() {

        Set<ResultAndProject> paymentContainerSet;

        try {
            paymentContainerSet = paymentService.successfulResults();
            if (paymentContainerSet.isEmpty()) {
                throw new RuntimeException();
            }
        } catch (RuntimeException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.ok(paymentContainerSet);
    }

    @GetMapping("/long-compilation")
    public ResponseEntity<? super PaymentContainer> getLongCompilation() {

        Set<PaymentContainer> paymentContainerSet;

        try {
            paymentContainerSet = paymentService.longCompilation();
            if (paymentContainerSet.isEmpty()) {
                throw new RuntimeException();
            }
        } catch (RuntimeException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.ok(paymentContainerSet);
    }

    @GetMapping("/normal-compilation")
    public ResponseEntity<? super PaymentContainer> getNormalCompilation() {

        Set<PaymentContainer> paymentContainerSet;

        try {
            paymentContainerSet = paymentService.normalCompilation();
            if (paymentContainerSet.isEmpty()) {
                throw new RuntimeException();
            }
        } catch (RuntimeException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.ok(paymentContainerSet);
    }

    @PostMapping(value = "/register-compilation-time")
    public ResponseEntity<? super PaymentContainer> registerPayment(@RequestBody PaymentContainer paymentContainer) {

        boolean registeredResult = paymentService.register(paymentContainer);

        if (!registeredResult) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        ResponseEntity<PaymentContainer> paymentContainerResponseEntity = new ResponseEntity<>(paymentContainer, HttpStatus.OK);

        return ResponseEntity.ok(paymentContainer);
    }
}
