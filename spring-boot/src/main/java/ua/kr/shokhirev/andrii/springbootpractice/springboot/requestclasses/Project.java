package ua.kr.shokhirev.andrii.springbootpractice.springboot.requestclasses;

public class Project {

    private String projectName;

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
}
