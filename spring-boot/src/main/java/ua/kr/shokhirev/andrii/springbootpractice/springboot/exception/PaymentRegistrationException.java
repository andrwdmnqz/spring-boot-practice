package ua.kr.shokhirev.andrii.springbootpractice.springboot.exception;

public class PaymentRegistrationException extends RuntimeException {

    public PaymentRegistrationException(String message) {
        super(message);
    }
}
