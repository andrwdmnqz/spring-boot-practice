package ua.kr.shokhirev.andrii.springbootpractice.springboot.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class PaymentExceptionHandler {

    @ExceptionHandler(PaymentRegistrationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void handlePaymentException() {

    }
}
