package ua.kr.shokhirev.andrii.springbootpractice.springboot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.kr.shokhirev.andrii.springbootpractice.springboot.container.PaymentContainer;
import ua.kr.shokhirev.andrii.springbootpractice.springboot.exception.PaymentRegistrationException;
import ua.kr.shokhirev.andrii.springbootpractice.springboot.repository.PaymentRepository;
import ua.kr.shokhirev.andrii.springbootpractice.springboot.requestclasses.ResultAndProject;
import ua.kr.shokhirev.andrii.springbootpractice.springboot.requestclasses.Project;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class PaymentService {

    @Autowired
    private PaymentRepository paymentRepository;

    public int getCount() {
        return paymentRepository.getAll().size();
    }

    public Set<Project> getAllNamesList() {

        Set<PaymentContainer> paymentContainerSet = paymentRepository.getAll();
        Set<Project> results = new HashSet<>();

        Iterator<PaymentContainer> paymentContainerIterator = paymentContainerSet.iterator();

        while (paymentContainerIterator.hasNext()) {
            Project result = new Project();
            result.setProjectName(paymentContainerIterator.next().getProject());
            results.add(result);
        }

        return results.stream()
                .filter(distinctByKey(p -> p.getProjectName()))
                .collect(Collectors.toSet());
    }

    public Set<ResultAndProject> unsuccessfulResults() {

        Collection<PaymentContainer> paymentContainerSet = paymentRepository.getAll();
        Set<ResultAndProject> failedResults = new HashSet<>();

        Iterator<PaymentContainer> failedResultsIterator = paymentContainerSet.iterator();

        while (failedResultsIterator.hasNext()) {
            ResultAndProject newFailedResult = new ResultAndProject();

            PaymentContainer temp = failedResultsIterator.next();

            newFailedResult.setResult(temp.getResult());
            newFailedResult.setProjectName(temp.getProject());
            failedResults.add(newFailedResult);
        }

        return failedResults.stream()
                .filter(p -> !p.getResult().equals("success"))
                .collect(Collectors.toSet());

//        String paymentContainerString = paymentContainerSet.stream()
//                .filter(p -> !p.getResult().equals("success"))
//                .collect(Collectors.mapping(PaymentContainer::toString, Collectors.joining(", ")));
//        return paymentContainerString;
    }

    public Set<ResultAndProject> successfulResults() {

        Collection<PaymentContainer> paymentContainerSet = paymentRepository.getAll();
        Set<ResultAndProject> successResults = new HashSet<>();

        Iterator<PaymentContainer> successResultIterator = paymentContainerSet.iterator();

        while (successResultIterator.hasNext()) {
            ResultAndProject newSuccessResult = new ResultAndProject();

            PaymentContainer temp = successResultIterator.next();

            newSuccessResult.setResult(temp.getResult());
            newSuccessResult.setProjectName(temp.getProject());
            successResults.add(newSuccessResult);
        }

        return successResults.stream()
                .filter(p -> p.getResult().equals("success"))
                .collect(Collectors.toSet());

//        Collection<PaymentContainer> paymentContainerSet = paymentRepository.getAll();
//        String paymentContainerString = paymentContainerSet.stream()
//                .filter(p -> p.getResult().equals("success"))
//                .collect(Collectors.mapping(PaymentContainer::toString, Collectors.joining(", ")));
//        return paymentContainerString;
    }

    public Set<PaymentContainer> longCompilation() {

        Set<PaymentContainer> paymentContainerSet = paymentRepository.getAll();

        return paymentContainerSet.stream()
                .filter(p -> p.getCompilationTime() >= 1000)
                .collect(Collectors.toSet());

//        String paymentContainerString = paymentContainerSet.stream()
//                .filter(p -> p.getCompilationTime() >= 1000)
//                .collect(Collectors.mapping(PaymentContainer::toString, Collectors.joining(", ")));
//        System.out.println(paymentContainerString);

    }

    public Set<PaymentContainer> normalCompilation() {

        Collection<PaymentContainer> paymentContainerSet = paymentRepository.getAll();

        return paymentContainerSet.stream()
                .filter(p -> p.getCompilationTime() < 1000)
                .collect(Collectors.toSet());

//        String paymentContainerString = paymentContainerSet.stream()
//                .filter(p -> p.getCompilationTime() < 1000)
//                .collect(Collectors.mapping(PaymentContainer::toString, Collectors.joining(", ")));
//        System.out.println(paymentContainerString);
//        return paymentContainerString;
    }

    public boolean register(PaymentContainer paymentContainer) {

        Set<PaymentContainer> paymentContainerSet = paymentRepository.getAll();

        Iterator<PaymentContainer> paymentContainerIterator = paymentContainerSet.iterator();

        while (paymentContainerIterator.hasNext()) {
            if (paymentContainerIterator.next().equals(paymentContainer)) {
                throw new PaymentRegistrationException(String.format("Operation %s is already exists!", paymentContainer.getTimestamp()));
            }
        }

        if (paymentContainer.getProject() == null || paymentContainer.getCompilationTime() == 0 ||
                paymentContainer.getResult() == null || paymentContainer.getTimestamp() == null) {
            return false;
        }

        paymentRepository.add(paymentContainer);

        return true;
     }

    public static <T> Predicate<T> distinctByKey(
            Function<? super T, ?> keyExtractor) {

        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }
}
