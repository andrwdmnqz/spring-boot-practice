package ua.kr.shokhirev.andrii.springbootpractice.springboot.container;

import java.util.Objects;

public class PaymentContainer {
    private String project;
    private String timestamp;
    private String compilationTime;
    private String result;

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public int getCompilationTime() {
        return Integer.parseInt(compilationTime);
    }

    public void setCompilationTime(String compilationTime) {
        this.compilationTime = compilationTime;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

//    public boolean isNullFields() {
//        if (this.project == null || this.result == null || this.compilationTime == null || this.timestamp == null) {
//            return true;
//        }
//        return false;
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PaymentContainer that = (PaymentContainer) o;
        return Objects.equals(project, that.project) && Objects.equals(timestamp, that.timestamp) && Objects.equals(compilationTime, that.compilationTime) && Objects.equals(result, that.result);
    }

    @Override
    public String toString() {
        return "PaymentContainer{" +
                "project='" + project + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", compilationTime='" + compilationTime + '\'' +
                ", result='" + result + '\'' +
                '}';
    }
}